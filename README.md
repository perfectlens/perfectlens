Here at Perfectlens.ca, we offer your favourite brand name contact lenses at the most affordable prices. With so many choices in ordering contact lenses online, it can be hard to choose. We've simplified this process by providing the LOWEST prices in Canada on contact lenses. The top brands at the lowest price.

We are people, like you, who used to wear glasses and have now made the switch for a more comfortable vision experience with contact lenses!

Our company is proudly Canadian and is based out of Vancouver, BC. We have a rapidly growing team who are dedicated to bring you the best products, the lowest prices, the most reliable information and outstanding customer support.